package protocol

import (
   "strings"
   "strconv"
   //"github.com/ugorji/go/codec"
)

const (
    NUL    = 0x00 // NULL
    SOH    = 0x01 // Start of Header
    STX    = 0x02 // Start of TeXt
    ETX    = 0x03 // End of TeXt
    EOT    = 0x04 
    ETB    = 0x17 // End of Transmission block
    PUT    = "put"
    GET    = "get"
    DELETE  = "delete"
    ERR = "error"
)


type Message struct {
    I uint64
    C string
    K []byte
    B []byte
}

// var mh codec.MsgpackHandle

// func Decode(buf []byte)(msg Message, err error){
    
//     //var mh codec.MsgpackHandle
//     dec := codec.NewDecoderBytes(buf, &mh)
//     err = dec.Decode(&msg)
//     return msg, err
// }

// func Encode(msg Message)(buf []byte, err error){
    
//     //var mh codec.MsgpackHandle
//     enc := codec.NewEncoderBytes(&buf, &mh)
//     err = enc.Encode(msg)
//     return buf, err
// }


func Deserialize(msg string)(m Message, errm error){ 
    
    chunks := strings.Split(msg, string(SOH))
    length := len(chunks)
    
    if length > 0{
        u, err := strconv.ParseUint(chunks[0], 10, 64)
        if err != nil{
            errm = err
            return
        } 
        m.I = u
    }
    
    if length > 1{
        m.C = chunks[1]
    }
    
    if length > 2{
        m.K = []byte(chunks[2])
    }
    
    if length > 3{
        m.B = []byte(chunks[3])
    }
    
    return m, nil
}

func Serialize(m Message)[]byte{
    
    id := append([]byte(strconv.FormatUint(m.I, 10)), SOH)
    command := append([]byte(m.C), SOH)
    key := append(m.K, SOH)
    body := append(m.B, SOH)
    
    data := append(id, command...)
    data = append(data, key...)
    data = append(data, body...)
    
    return data
}


// TODO: work in progress to make binary protocol
// type Packet struct {
//    chunks []string
// }

// func ParsePacket(msg string)(p *Packet, err error){
//     p = new(Packet)
//     chunks := strings.Split(msg, string(SOH))
//     if len(chunks) == 5{ 
//         p.chunks = chunks
//     }else{
//         return p, errors.New("ParsePacket: invalid number of sections")
//     }
//     return p, nil 
// }

// func NewPacket()(*Packet){
//     p := new(Packet)
//     p.chunks = []string{"","","",""}
//     return p 
// }

// func (p *Packet) Id()string{
//     return p.chunks[0]
// }

// func (p *Packet) Command()string{
//     return p.chunks[1]
// }

// func (p *Packet) Key()string{
//     return p.chunks[2]
// }

// func (p *Packet) Body()string{
//     return p.chunks[3]
// }

// func (p *Packet) AddId(id string){
//     p.chunks[0] = id
// }

// func (p *Packet) AddCommand(command string){
//     p.chunks[1] = command
// }

// func (p *Packet) AddKey(key string){
//     p.chunks[2] = key
// }

// func (p *Packet) AddBody(body string){
//     p.chunks[3] = body
// }

// func (p *Packet) Put(id string, key string, body string){
//     p.AddId(id)
//     p.AddCommand(PUT)
//     p.AddKey(key)
//     p.AddBody(body)
// }

// func (p *Packet) Get(id string, key string){
//     p.AddId(id)
//     p.AddCommand(GET)
//     p.AddKey(key)
// }

// func (p *Packet) Delete(id string, key string){
//     p.AddId(id)
//     p.AddCommand(DELETE)
//     p.AddKey(key)
// }

// func (p *Packet) Success(){
//     p.AddBody("")
// }

// func (p *Packet) SuccessWithBody(body string){
//     p.AddBody(body)
// }

// func (p *Packet) Error(err string){
//     p.AddBody(err)
// }

// var del string = string(SOH)

// func (p *Packet) ToMessage() []byte {
    
//     // id := append([]byte(p.Id()), SOH)
//     // command := append([]byte(p.Command()), SOH)
//     // //key := append([]byte(p.Key()), SOH)
//     // //body := append([]byte(p.Body()), SOH)
    
//     // data := append(id, command...)
//     // //data = append(data, key...)
//     // //data = append(data, body...)
    
//     // return data
    
//     // str := strings.Join(p.chunks, del)
//     // str = str + del
//     //del := string(SOH)
//     // str := ""
//     // for i := 0; i < len(p.chunks); i++ {
//     //     str += p.chunks[i] + del
//     // }
//     // del := string(SOH)
    
//     str := p.chunks[0] + del
//     str += p.chunks[1] + del
//     str += p.chunks[2] + del
//     str += p.chunks[3] + del
    
//     return []byte(str)
// }
