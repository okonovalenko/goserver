package main

import (
    "net"
    "os"
    "fmt"
    "time"
    "bufio"
    "./utils"
    "io/ioutil"
    "flag"
    "./protocol"
    "strconv"
    "github.com/ugorji/go/codec"
    //b64 "encoding/base64"
)

var (
    HOST = "localhost"
    PORT = "9838"
    TYPE = "tcp"
    LIMIT = 1
    THREADS = 1
    PUT = true
    GET = false
    DELETE = false
    DEBUG = false
)

var mh codec.MsgpackHandle

func main() {
    
    handleArguments()
    
    for i := 0; i < THREADS; i++ {
       go createConnection()
    }
    
    println("press enter to exit:")
    
    fmt.Scanln()
    
}

func createConnection(){
    
    tcpAddr, err := net.ResolveTCPAddr(TYPE, HOST + ":" + PORT)
    if err != nil {
        println("ResolveTCPAddr failed:", err.Error())
        os.Exit(1)
    }

    conn, err := net.DialTCP(TYPE, nil, tcpAddr)
    if err != nil {
        println("Dial failed:", err.Error())
        os.Exit(1)
    }

    go send(conn)
    
    go recv(conn)
}

func readPayload()[]byte{
    payload, err := ioutil.ReadFile("payload.dat")
    if err != nil {
       println("Error reading payload.dat:", err.Error())
    }
    return payload
}

func send(conn net.Conn){
    defer utils.TimeTrack(time.Now(), "send")

    payload := readPayload()
    println("payload size:", len(payload))

    var totalPacketsSent int = 0
    
    for i := 0; i < LIMIT / THREADS; i++ {

        id := strconv.Itoa(i)
        key := []byte(utils.GenerateKey(id))
        
        if PUT {
            msg := protocol.Message{I: uint64(i), C: "put", K: key, B: payload}
            buf, _ := Encode(msg)
            
            write(conn, buf)
        }
        
        if GET {
            msg := protocol.Message{I: uint64(i), C: "get", K: key}
            buf, _ := Encode(msg)
            
            write(conn, buf)
        }
        
        if DELETE {
            msg := protocol.Message{I: uint64(i), C: "delete", K: key}
            buf, _ := Encode(msg)
            
            write(conn, buf)
        }
        
        totalPacketsSent++
    }
    
    println("total packets sent", totalPacketsSent)
}

func write(conn net.Conn, data []byte){
    
    //dst := make([]byte, b64.StdEncoding.EncodedLen(len(data)))
    
    //b64.StdEncoding.Encode(dst, data)
    
    buf := append(data, protocol.ETB)
    
    _, err := conn.Write(buf)
    if err != nil {
        println("Write failed:", err.Error())
        os.Exit(1)
    }
}

var totalPacketsReceived = 0

func recv(conn net.Conn){
    defer utils.TimeTrack(time.Now(), "recv")
    defer conn.Close()
    
    totalPacketsReceived := 0
    connbuf := bufio.NewReader(conn)
    
    for{
        str, err := connbuf.ReadString(protocol.ETB)
        if err != nil {
            println("Read Bytes failed:", err.Error())
            os.Exit(1)
        }
      
        //buf, _ := b64.StdEncoding.DecodeString(str)
        //buf := []byte(str)
        msg, err := Decode(str)
        if err != nil {
            break
        }
       
        // print message content
        if DEBUG {
            println(msg.I, msg.C, string(msg.K), msg.B)
        }
        
        totalPacketsReceived++
        
        if LIMIT / THREADS == totalPacketsReceived{
            break
        }
    }
    
    println("total packets received", totalPacketsReceived)
}

func handleArguments(){
    flag.StringVar(&HOST,"host", "localhost", "Host to connect to")
    flag.StringVar(&PORT, "port", "9838", "Port to connect on")
    flag.BoolVar(&PUT, "put", false, "Use put command to run payload")
    flag.BoolVar(&GET, "get", false, "Use get command to run payload")
    flag.BoolVar(&DELETE, "delete", false, "Use delete command to run payload")
    flag.BoolVar(&DEBUG, "debug", false, "Use this flag to print responses from each operation")
    flag.IntVar(&LIMIT, "records", 1, "Number of operations to run") 
    flag.IntVar(&THREADS, "threads", 1, "Number of threads to use, each thread will create a tcp connection") 
    flag.Parse()
}

func Decode(str string)(msg protocol.Message, err error){
    // dec := codec.NewDecoderBytes(buf, &mh)
    // err = dec.Decode(&msg)
    
    msg, err = protocol.Deserialize(str)
    
    return msg, err
}

func Encode(msg protocol.Message)(buf []byte, err error){
    // enc := codec.NewEncoderBytes(&buf, &mh)
    // err = enc.Encode(msg)
    
    buf = protocol.Serialize(msg)
    
    return buf, err
}
