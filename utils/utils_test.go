package utils

import (
    "testing"
)

func TestGenerateKey(t *testing.T) {
  hash := GenerateKey("some data")
  if len(hash) != 40 {
    t.Error("Expected 40 bytes character hash", hash)
  }
}