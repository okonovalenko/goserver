package utils

import (
    "time"
    "log"
    "crypto/sha1"
    "encoding/hex"
)

func TimeTrack(start time.Time, name string) {
    elapsed := time.Since(start)
    log.Printf("%s took %s", name, elapsed)
}

func GenerateKey(str string)(id string) {
    h := sha1.New()
    data := []byte(str)
    h.Write(data)
    id = hex.EncodeToString(h.Sum(nil))
    return
}
