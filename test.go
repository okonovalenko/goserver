package main

import (
	"io"
	"net/http"
)

func hello(w http.ResponseWriter, r *http.Request) {
	io.WriteString(w, "loaderio-28db60ae1e90b953b37b7d4bf3d7fd33")
}

func token(w http.ResponseWriter, r *http.Request) {
	io.WriteString(w, "loaderio-28db60ae1e90b953b37b7d4bf3d7fd33")
}

func main() {
	
	http.HandleFunc("/loaderio-28db60ae1e90b953b37b7d4bf3d7fd33", token)
	http.HandleFunc("/", hello)
	http.ListenAndServe(":8000", nil)
}