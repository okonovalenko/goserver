package main

import (
    "fmt"
    "net"
    "os"
    "io"
    "time"
    "bufio"
    "./utils"
    "github.com/syndtr/goleveldb/leveldb"
    "github.com/syndtr/goleveldb/leveldb/opt"
    "flag"
    "bytes"
    "./protocol"
    "github.com/ugorji/go/codec"
    //b64 "encoding/base64"
)

var (
    IP = "127.0.0.1"
    PORT = "9838"
    TYPE = "tcp"
    LEVELDB = "db"
    LEVELDB_WRITE_BUFFER = 128
    MEMORY = false
    DEBUG = false
)

var mh codec.MsgpackHandle


func main() {
    
    handleArguments()
    
    // listen for incoming connections.
    listen := initializeListener()
    defer listen.Close()
    
    // initialize leveldb and memory store
    //memDb := &MemoryStore{m: make(map[string]string)}
    levelDb := initializeDbConnection()    
    defer levelDb.Close()
    
    for {
        // wait for an incoming connection.
        conn := waitForConnection(listen)
        // handle connections in a new goroutine.
        go handleConnection(conn, levelDb)
    }
}

func waitForConnection(listen net.Listener)(conn net.Conn){
    conn, err := listen.Accept()
    if err != nil {
        fmt.Println("Error accepting: ", err.Error())
        os.Exit(1)
    }
    return conn
}

func initializeListener()(net.Listener){  
    listen, err := net.Listen(TYPE, IP+":"+PORT)
    if err != nil {
        fmt.Println("Error listening:", err.Error())
        os.Exit(1)
    }
    fmt.Println("Server listening on " + IP + ":" + PORT)
    return listen
}

func initializeDbConnection()(*leveldb.DB){
    options := &opt.Options{ WriteBuffer: 1024 * 1024 * LEVELDB_WRITE_BUFFER }
    db, err := leveldb.OpenFile(LEVELDB, options)
    if err != nil {
        println("Can't open leveldb:", err.Error())
        os.Exit(1)
    }
    fmt.Println("Leveldb database initialized, path: " + LEVELDB) 
    return db
}

func handleConnection(conn net.Conn, levelDb *leveldb.DB) {
    defer utils.TimeTrack(time.Now(), "handleConnection") 
         
    connbuf := bufio.NewReader(conn)

    for {
        
        //readMessage(conn)
        
        // receive data from socket
        str, err := connbuf.ReadString(protocol.ETB)
        if err != nil {
            break
        }
        
        //println(str)
        //buf, _ := b64.StdEncoding.DecodeString(str)
        //buf := []byte(str)
        msg, err := Decode(str)
        if err != nil {
            break
        }
       
        // print message content
        if DEBUG {
            println(msg.I, msg.C, string(msg.K), msg.B)
        }
        
        handleCommand(conn, levelDb, msg)
        
        // msg.K = []byte{}
        // msg.B = []byte{}
    
        // resp(conn, msg)
    }
}

func readMessage(conn net.Conn){
    
    buf := make([]byte, 0, 4096) // big buffer
    tmp := make([]byte, 256)     // using small tmo buffer for demonstrating
    for {
        n, err := conn.Read(tmp)
        if err != nil {
            if err != io.EOF {
                fmt.Println("read error:", err)
            }
            break
        }
        
        buf = append(buf, tmp[:n]...)
        
        ind := bytes.IndexByte(buf, protocol.ETB)
        if ind > -1 { 
            println("found packet:", ind)
        }

    }
    
}

func handleCommand(conn net.Conn, db *leveldb.DB, p protocol.Message){
    switch p.C {
        case protocol.PUT:
            handlePutCommand(conn, db, p)
            break
        case protocol.GET:
            handleGetCommand(conn, db, p)
            break
        case protocol.DELETE:
            handleDeleteCommand(conn, db, p)
            break
    }
}

func handlePutCommand(conn net.Conn, db *leveldb.DB, m protocol.Message){   
    err := db.Put(m.K, m.B, nil)
    if err != nil {
        m.C = protocol.ERR
        m.B = []byte(err.Error())
    }else{
        m.K = []byte{}
        m.B = []byte{}
    }
    resp(conn, m)
}

func handleGetCommand(conn net.Conn, db *leveldb.DB, m protocol.Message){
    data, err := db.Get(m.K, nil)
    if err != nil {
        m.C = protocol.ERR
        m.B = []byte(err.Error())
    }else{
        m.B = data
    }
    resp(conn, m)
}

func handleDeleteCommand(conn net.Conn, db *leveldb.DB, m protocol.Message){
    err := db.Delete(m.K, nil)
    if err != nil {
        m.C = protocol.ERR
        m.B = []byte(err.Error())
    }else{
        m.K = []byte{}
        m.B = []byte{}
    }
    resp(conn, m)
}

func resp(conn net.Conn, m protocol.Message){
    data, _ := Encode(m)
    
    //dst := make([]byte, b64.StdEncoding.EncodedLen(len(data)))
    //b64.StdEncoding.Encode(dst, data)    
    buf := append(data, protocol.ETB)
    
    _, err := conn.Write(buf)
    if err != nil {
        println("Write failed:", err.Error())
    }
}

func handleArguments(){
    flag.StringVar(&IP,"ip", "127.0.0.1", "IP address to listen on")
    flag.StringVar(&PORT, "port", "9838", "Port to listen on")
    flag.StringVar(&LEVELDB, "leveldb", "db", "Path to leveldb database") 
    flag.BoolVar(&MEMORY, "memory", false, "Set this flag if you want to run in memory storage only, no data will be persisted to disk") 
    flag.BoolVar(&DEBUG, "debug", false, "Set this flag to output messages to console") 
    flag.IntVar(&LEVELDB_WRITE_BUFFER, "wbuf", 128, "Leveldb write buffer size in MB, higher size can improve performance") 
    flag.Parse()
}

func Decode(str string)(msg protocol.Message, err error){
    // dec := codec.NewDecoderBytes(buf, &mh)
    // err = dec.Decode(&msg)
    
    msg, err = protocol.Deserialize(str)
    
    return msg, err
}

func Encode(msg protocol.Message)(buf []byte, err error){
    // enc := codec.NewEncoderBytes(&buf, &mh)
    // err = enc.Encode(msg)
    
    buf = protocol.Serialize(msg)
    
    return buf, err
}