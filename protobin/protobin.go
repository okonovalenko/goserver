package protobin

import (
   "bytes"
   "encoding/binary"
)

const (
    NUL    = 0x00 // NULL
    SOH    = 0x01 // Start of Header
    STX    = 0x02 // Start of TeXt
    ETX    = 0x03 // End of TeXt
    EOT    = 0x04 
    PUT    = 0x05
    GET    = 0x06
    DELETE = 0x07
    DOT    = 0x14
    ETB    = 0x17 // End of Transmission block
    PIP    = 0x7C
)

type Message struct {
    data []byte
}

func NewMessage(buf []byte)(*Message){
    m := new(Message)
    m.data = buf
    return m 
}

func (m *Message) Id()uint64{
    return ByteToUint64(m.data[:8])
}

func (m *Message) Command()byte{
    return m.data[8]
}

func (m *Message) Key()[]byte{
    return m.data[9:49]
}

func (m *Message) Body()[]byte{
    return m.data[49:]
}

func (m *Message) AddId(id uint64){
    m.data = append(m.data, Uint64ToByte(id)...)
}

func (m *Message) AddCommand(command byte){
    m.data = append(m.data, command)
}

func (m *Message) AddKey(key []byte){
    m.data = append(m.data, key...)
}

func (m *Message) AddBody(body []byte){
    m.data = append(m.data, body...)
}

func (m *Message) Put(key []byte, body []byte, id uint64){
    m.AddId(id)
    m.AddCommand(PUT)
    m.AddKey(key)
    m.AddBody(body)
}

func (m *Message) Get(key []byte, id uint64){
    m.AddId(id)
    m.AddCommand(GET)
    m.AddKey(key)
}

func (m *Message) Delete(key []byte, id uint64){
    m.AddId(id)
    m.AddCommand(DELETE)
    m.AddKey(key)
}

func (m *Message) ToBuffer()[]byte{
    return m.data
}

func Uint64ToByte(data uint64) []byte {
	buf := new(bytes.Buffer)
	binary.Write(buf, binary.BigEndian, data)
	return buf.Bytes()
}

func ByteToUint64(data []byte) uint64 {
	var value uint64
	buf := bytes.NewReader(data)
	binary.Read(buf, binary.BigEndian, &value)
	return value
}