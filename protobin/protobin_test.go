package protobin

import (
    "testing"
    "strconv"
    "../utils"
    //"bytes"
    //"bufio"
   "github.com/ugorji/go/codec"
)


const payload = `{"field2" : "KkUrJCQkKC48KF1rK0crNENjPEE9MEZ5PjYsOC5qNVcrLVc5NCM0NEYpOUw5JCEuMkk9ITpsMCgwKUVvOkozKF85IzwkIFxjL05xPzRqLlYjNVc3ODsgLDUgNCRqP1sxO1F7Jg==",
 "field9" : "I1s/LV47ITYwJTt+PVNhL0J/Jj0wNSN4JzImLUthOi16JlM7Lkt9PkB9I0RrKyg4PVl3I099J1l9KkpvO005JUJ5Ojo6KEc7MDQmNTk+KThgOlhrPF1pJk4nPDYwIE9lPyxuJw==",
 "field8" : "JzY0MUp1NE8vNU91N1lxI187PDx4JjNiKkU/LTIkNEtrIkchKyU8NE9hMDR2LiwqLlRzN1tnISQ8ISouPTowOD80ISV6P0x9Nj56JzsmNFt7LkxlKyNoJ0clN0x3KzliIFV1Kw==",
 "field4" : "IT9oIiIgIEIlKUIlPCQ4KEhrOSE2LCdiOy8yJFAxKFEtPEJnIzJmLzt4M0Y/LF5jN0ExPUdpJkl7NzMmKzZ+OiMmNVNlMkktLkklNCN4JVBrKzx2IyUoLFo7Mjk+MTgkJz4kJQ==",
 "field6" : "LFVhO0xrMTloK1UrPTQ8MFR9NExvMkYjNjUiNDkwLk5xIk49O1NhKzMyPDtyMzNiIko9JE0lNVg1PC0qOjUqLC1kO0Q7OTx+JkAlNUVhPSJwMlIvIiU8NFshJTsyMTpiKl1zMw==",
 "field1" : "IyxiPzA0KSouJFcjMSgwJFhrM0p5M0t7LEd1PzIwLDpmLkBrNDl2Jjc4PUtpPlEhKV5lPigkO107JDQ0K10lJEZjLUBzKjIiPDQkJUl/NTIuJVE5PjU+OCs2Jk1xMiQ8O08zJA==",
 "field0" : "NCc4JU5tITx4OlUnOlF9Mil0NFwnIFMjKlI5MT5wPTNwPytqJEd1Njw+MTt+NilqLT5mMkljMFQlOE0tNSc8PC4oO0slNUJxMFd/KywwMUA1LDZqLEItJFo7JFplK0ljM0wtKA==",
}`

func TestPut(t *testing.T) {
    
    id := strconv.Itoa(0)
    key := []byte(utils.GenerateKey(id))
    p := []byte(payload)
    
    for i := 0; i < 10; i++ {
        
        m := new(Message)
        m.Put(key, p, uint64(i))
        data := m.ToBuffer()
        
        m2 := NewMessage(data)
        
        //println(string(data))        
        if m2.Id() != uint64(i) {
            t.Error("Expected bytes character hash")
        }
    }
}


func BenchmarkPut(b *testing.B) {
    
    id := strconv.Itoa(0)
    key := []byte(utils.GenerateKey(id))
    p := []byte(payload)
    
    for i := 0; i < b.N; i++ {
        
        m := new(Message)
        m.Put(key, p, uint64(i))
        // data := m.ToBuffer()
        
        // m2 := NewMessage(data)
        
        // //println(string(data))        
        // if m2.Id() != uint64(i) {
        //    b.Error("Expected bytes character hash")
        // }
        
        // _ = m2.Command()
        // _ = m2.Key()
        // _ = m2.Body()
    }
}

type Msg struct{
    Id uint64
    Command []byte
    Key []byte
    Body []byte
}

func TestMsgPck(t *testing.T) {
    id := strconv.Itoa(0)
    key := []byte(utils.GenerateKey(id))
    p := []byte(payload)
    
    var (
        v1 = Msg{Id:0, Command: []byte("put"), Key: key, Body: p}
        v2 = Msg{}
        buf []byte
        mh codec.MsgpackHandle
    )
    
    enc := codec.NewEncoderBytes(&buf, &mh)
    _ = enc.Encode(v1)
    //println("Encoded buffer:", string(buf))
    
    
    dec := codec.NewDecoderBytes(buf, &mh)
    _ = dec.Decode(&v2)
    //println("Decoded:", v2.Id, v2.Command, string(v2.Key))
    
    if len(v1.Key) != len(v2.Key) {
        t.Error("Expected the same key")
    }
}

func BenchmarkMsgPck(b *testing.B) {
    // id := strconv.Itoa(0)
    // key := []byte(utils.GenerateKey(id))
    // p := []byte(payload)
    
    var (
        //v1 = Msg{Id:0, Command: []byte("put"), Key: key, Body: p}
        v1 = Msg{Id:0}
        
        buf []byte
        mh codec.MsgpackHandle
    )
    
    enc := codec.NewEncoderBytes(&buf, &mh)
    dec := codec.NewDecoderBytes(buf, &mh)
    
    for i := 0; i < b.N; i++ {
        
        _ = enc.Encode(v1)
        //println("Encoded buffer:", string(buf))
        
        var v2 = Msg{}
        _ = dec.Decode(&v2)
        //println("Decoded:", v2.Id, v2.Command, v2.Key)
    }
    
}
